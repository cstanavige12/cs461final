﻿using Final.Abstract;
using Final.Models;
using Final.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final.Controllers
{
    class BaseController : EventListener
    {
        private Model model;
        private View view;

        public BaseController()
        {

        }

        public void ProcessEvent()
        {
            throw new NotImplementedException();
        }
    }
}
